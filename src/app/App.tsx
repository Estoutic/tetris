import './App.css'
import React from 'react'
import { createBrowserRouter, createRoutesFromElements, Route, Router, RouterProvider, Routes } from 'react-router-dom'
import GamePage from '../pages/GamePage/GamePage'

function App() {


  const router = createBrowserRouter(
    createRoutesFromElements(
      <Route path='/' element= {<GamePage/>}>
         </Route>
    )
  )
  return (<> 
      <RouterProvider router={router}/>
    </>
  )
}

export default App
