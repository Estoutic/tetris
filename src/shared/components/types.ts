import { HtmlHTMLAttributes } from "react";

export interface IBaseComponentProps extends HtmlHTMLAttributes<HTMLElement>{
    ref?: any
}