import React from "react";
import { IBaseComponentProps } from "../types";
import "./NavBar.css"
type Props = IBaseComponentProps;

const NavBar = ({className, ...rest} : Props) => {
    const classes = ["nav-bar", className];

    return(

        <nav {...rest} className={classes.join(" ")}>
            <a href="/">Game</a>
            <a href="/">Profile</a>

            <a href="/">Contact</a>

        </nav>
    )
}

export default NavBar;