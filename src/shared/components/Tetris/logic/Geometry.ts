import Shape from "./Shape";

const shapes: Shape[] = [];

shapes.push(
  new Shape([
    [0, 1, 0],
    [0, 1, 0],
    [1, 1, 0],
  ]),
  new Shape([
    [0, 0, 0],
    [1, 1, 1],
    [0, 1, 0],
  ]),
  new Shape([
    [0, 1, 0],
    [0, 1, 0],
    [0, 1, 1],
  ]),
  new Shape([
    [0, 0, 0],
    [0, 1, 1],
    [1, 1, 0],
  ]),
  new Shape([
    [0, 1, 0],
    [0, 1, 0],
    [1, 1, 0],
  ]),
  new Shape([
    [0, 0, 1, 0],
    [0, 0, 1, 0],
    [0, 0, 1, 0],
    [0, 0, 1, 0],
  ]),
  new Shape([
    [1, 1],
    [1, 1],
  ]),
  new Shape([
    [0, 0, 0],
    [1, 1, 0],
    [0, 1, 1],
  ]),

);

export default shapes;
