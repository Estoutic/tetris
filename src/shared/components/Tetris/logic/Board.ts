import Block from "../entities/Block";
import Shape from "./Shape";

const rows: number = 20;
const columns: number = 10;

let board: Block[][] = new Array(rows);
let gameOver: boolean;
let currentShape: Shape;
let nextShape: Shape;
let gameSpeed = 5;

fillBoard();

function fillBoard() {
  for (let index = 0; index < board.length; index++) {
    board[index] = new Array(columns);

    for (let jindex = 0; jindex < columns; jindex++) {
      board[index][jindex] = new Block("block element");
    }
  }
}

export function getBoard() {
  return board;
}
