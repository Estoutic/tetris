class Shape {
  template: number[][];

  constructor(template: number[][]) {
    this.template = template;
  }
}

export default Shape;
