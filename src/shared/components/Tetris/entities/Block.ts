class Block {
  type: string;

  constructor(type: string) {
    this.type = type;
  }
}

export default Block;