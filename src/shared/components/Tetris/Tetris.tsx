import React, { useEffect } from "react";
import { IBaseComponentProps } from "../types";
import { getBoard } from "./logic/Board";
import { handleKeyPress } from "./logic/keyHandler";
import "./Tetris.css";
type Props = IBaseComponentProps;

const Tetris = ({ className, ...rest }: Props) => {
  const classes = ["tetris-container", className].join(" ");
  let board = getBoard();

  useEffect(() => {
    const onKeyDown = (event) => {
      event.preventDefault();
      handleKeyPress(event);
    };

    window.addEventListener("keydown", onKeyDown);
    return () => {
      window.removeEventListener("keydown", onKeyDown);
    };
  }, []);

  return (
    <div className={classes}>
      <h1>TETRIS</h1>
      <div className="board">
        {board.map((row, rowIndex) => (
          <div key={rowIndex} id={rowIndex.toString()} className="row">
            {row.map((block, colIndex) => (
              <div
                key={colIndex}
                id={colIndex.toString()}
                className={`${block.type}`}
              ></div>
            ))}
          </div>
        ))}
      </div>
    </div>
  );
};

export default Tetris;
