import { IBaseComponentProps } from "@shared/components/types";
import React from "react";
import Tetris from "../../shared/components/Tetris/Tetris";

type Props = IBaseComponentProps;

const GamePage = ({ className, ...rest }: Props) => {
  const classes = ["game-page", className];
  return (
    <div {...rest} className={classes.join(" ")}>
      <Tetris></Tetris>
    </div>
  );
};

export default GamePage;
